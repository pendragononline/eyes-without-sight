var $ = function (id) { return document.getElementById(id); };
var mobileView = window.matchMedia("(max-width: 768px)");
var open = 0;
var searchOpen = 0;
var mobileSearchOpen = 0;

/*Show main navigation menu on mobile site*/
$('hamburger-button-text').addEventListener('click', function () {
    if (open === 0 && mobileSearchOpen === 0) {
        openMenu();
    }
    else if (open === 0 && mobileSearchOpen === 1) {
        closeMobileSearch();
        openMenu();
    }
    else if (open === 1) {
        closeMenu();
    }
});

function openMenu() {
    $('nav').classList.remove('hidden');
    $('nav').classList.add('visible');
    $('search-menu-button').classList.add('hidden');
    $('search-field').classList.add('hidden');
    $('search-button').classList.add('hidden');
    $('hamburger-button-text').innerHTML = "Close";
    $('analyse-button').addEventListener('click', closeMenu);
    $('landmark-button').addEventListener('click', closeMenu);
    $('celebrity-button').addEventListener('click', closeMenu);
    $('thumbnail-button').addEventListener('click', closeMenu);
    $('printedtext-button').addEventListener('click', closeMenu);
    $('handwriting-button').addEventListener('click', closeMenu);
    open = 1;
    if (mobileView.matches) {
        $('nav').setAttribute('aria-live', 'assertive');
        $('hamburger-button-text').setAttribute("alt", "Close navigation menu.");
    }
}

function closeMenu() {
    if (mobileView.matches) {
        $('nav').classList.remove('visible');
        $('nav').classList.add('hidden');
        $('search-menu-button').classList.remove('hidden');
        $('search-field').classList.remove('hidden');
        $('search-button').classList.remove('hidden');
        $('hamburger-button-text').setAttribute("alt", "Open navigation menu.");
        $('hamburger-button-text').innerHTML = "Menu";
        open = 0;
    }
}

/*Show search on mobile site*/
$('search-button-text').addEventListener('click', function () {
    if (mobileSearchOpen === 0 && open === 0) {
        openMobileSearch();
    }
    else if (mobileSearchOpen === 0 && open === 1) {
        closeMenu();
        openMobileSearch();
    }
    else if (mobileSearchOpen === 1) {
        closeMobileSearch();
    }
});

function openMobileSearch() {
    ariaHidden();
    $('nav').classList.remove('hidden');
    $('nav').classList.add('visible');
    $('main-layers').classList.add('hidden');
    $('search-layer').classList.remove('hidden');
    $('search-layer').classList.add('visible');
    $('search-field').focus();
    $('announce-search').innerHTML = "Search open - double tap to activate.";
    mobileSearchOpen = 1;
    if (mobileView.matches) {
        $('nav').setAttribute('aria-live', 'assertive');
        $('search-button-text').setAttribute("alt", "Close search panel");
        $('search-button-text').innerHTML = "Close";
    }
}

function closeMobileSearch() {
    if (mobileView.matches) {
        unhideAriaHidden();
        resetSearch();
        $('nav').classList.remove('visible');
        $('nav').classList.add('hidden');
        $('main-layers').classList.remove('hidden');
        $('search-layer').classList.add('hidden');
        $('search-button-text').innerHTML = "Search";
        mobileSearchOpen = 0;
    } 
}

function ariaHidden() {
    $('hamburger-button-text').setAttribute('aria-hidden', 'true');
    $('main-layers').setAttribute('aria-hidden', 'true');
}

function unhideAriaHidden() {
    $('hamburger-button-text').setAttribute('aria-hidden', 'false');
    $('main-layers').setAttribute('aria-hidden', 'false');
}

function resetSearch() {
    $('search-field').value = "Type to search";
}

/*Show search on desktop site*/
function showSearch() {
    if (searchOpen === 0) {
        $('search-layer').classList.remove('hidden');
        $('search-field').focus();
        searchOpen = 1;
    }
    else if (searchOpen === 1) {
        $('search-layer').classList.add('hidden');
        resetSearch();
        searchOpen = 0;
    }
}


