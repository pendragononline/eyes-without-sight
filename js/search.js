var $ = function (id) { return document.getElementById(id); };
var search = document.getElementById("search-field");
var keyTerms = ['grey', 'black', 'shirt', 'women', 'dress', 'casual', 'raglan', 't-shirt', 'kimono', 'womens', 'cart', 'basket', 'bag', 'womenswear', 'home', 'index', 'ffa', 'ribbed'];
var searchUsed = 0;

/*Enable enter button*/
var input = document.getElementById("search-field");
input.addEventListener("keyup", function (event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("search-button").click();
    }
});

/*Search algorithm*/
function searchFunction() {
    var searchTerm = search.value.toLowerCase();
    /*Find womenswear page*/
    if (searchTerm.includes(keyTerms[3]) || searchTerm.includes(keyTerms[13])) {
        window.location.href = "http://pendragon.online/demo/ffa/women.html";
    }
    /*Find black dress*/
    else if (searchTerm.includes(keyTerms[1]) || searchTerm.includes(keyTerms[4]) || searchTerm.includes(keyTerms[5]) || searchTerm.includes(keyTerms[17])) {
        window.location.href = "http://pendragon.online/demo/ffa/women/products/blackdress.html";
    }
    /*Find grey kimono shirt*/
    else if (searchTerm.includes(keyTerms[0]) || searchTerm.includes(keyTerms[2]) || searchTerm.includes(keyTerms[3])|| searchTerm.includes(keyTerms[5]) || searchTerm.includes(keyTerms[8])) {
        window.location.href = "http://pendragon.online/demo/ffa/women/products/kimonogreyshirt.html";
    }
    /*Find grey raglan shirt*/
    else if (searchTerm.includes(keyTerms[1]) || searchTerm.includes(keyTerms[3]) || searchTerm.includes(keyTerms[5]) || searchTerm.includes(keyTerms[6])) {
        window.location.href = "http://pendragon.online/demo/ffa/women/products/raglangreyshirt.html";
    }
    /*Find grey and black shirt*/
    else if (searchTerm.includes(keyTerms[0]) && searchTerm.includes(keyTerms[1]) || searchTerm.includes(keyTerms[2]) || searchTerm.includes(keyTerms[3]) || searchTerm.includes(keyTerms[5]) || searchTerm.includes(keyTerms[7])) {
        window.location.href = "http://pendragon.online/demo/ffa/women/products/blackgreyshirt.html";
    }
    /*Find cart*/
    else if (searchTerm.includes(keyTerms[10]) || searchTerm.includes(keyTerms[11]) || searchTerm.includes(keyTerms[12])) {
        window.location.href = "http://pendragon.online/demo/ffa/cart.html";
    }
    /*Find home*/
    else if (searchTerm.includes(keyTerms[14]) || searchTerm.includes(keyTerms[15]) || searchTerm.includes(keyTerms[16])) {
        window.location.href = "http://pendragon.online/demo/ffa/index.html";
    }
    else {
        search.value = "The item was not found";
    }
}

/*Clear search bar*/
function clearSearch() {
    search.value = "";
}

function keyPress() {
    if (search.value === "Type to search") {
        clearSearch();
    }
}

function clearSearchOnLoad() {
    if (search.value !== "Type to search") {
        clearSearch();
    }
}

clearSearchOnLoad();
